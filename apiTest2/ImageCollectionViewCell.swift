//
//  myViewCell.swift
//  apiTest2
//
//  Created by Hamza on 1/21/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
    var unsplash: Unsplash? {
        didSet {
            if let defualtLink = unsplash?.urls.small {
                imgView.checkForImageCache(url: defualtLink)
            }
        }
    }
   
    /*
     function for reusing selected cells of UiCollectionView
    */
    override func prepareForReuse() {
        imgView.image = nil
        super.prepareForReuse()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgView.contentMode = .scaleAspectFill
    }
}
    

