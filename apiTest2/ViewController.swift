import UIKit

class ViewController: UIViewController {
   
    let ClientID = "26ec142f27f85b3a86d24f95ad3e0811eaf7cbc92e7a7805ac224af8ea19eb8d"
    
    var unsplash = [Unsplash]()
    var isLoading = false
    var pageno = 1
    
    @IBOutlet weak var collectionView: UICollectionView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        let loadingReusableNib = UINib(nibName: "LoadingReusableView", bundle: nil)
        collectionView.register(loadingReusableNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "loadingresuableviewid")
        apicall()
    }
    
    /*
     LoadMoreData() checks condition for scrolling than call api
    */
    func loadMoreData() {
        if !self.isLoading {
            self.isLoading = true
            DispatchQueue.global().async {
                self.apicall()
            }
        }
    }

    /*
     This method fetches pages of images
     */
    func apicall() {
        let url = URL(string:"https://api.unsplash.com/photos?page=\(pageno)&per_page=30&client_id=\(ClientID)")
          URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error == nil {
                do {
                    let array = try JSONDecoder().decode(Array<Unsplash>.self, from: data!)
                    self.unsplash.append(contentsOf: array)
                    self.isLoading = false
                    self.pageno += 1
                    print("Image Cells Loaded = " , self.unsplash.count)
                }
                catch (let error) {
                    print(error)
                }
                
                DispatchQueue.main.async {
                  self.collectionView.reloadData()
                }
            }
        }.resume()
    }
    
   /*
    Segue for passing image to other viewController
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "def"{
            let menuController = segue.destination as! DetailViewController
            if let url = sender as? String {
                menuController.imageUrl = url
            }
        }
    }
}

extension ViewController :  UICollectionViewDelegate , UICollectionViewDataSource  {
    /*
    For Data Count and Data Populating
    */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.unsplash.count
     }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCollectionViewCell
        cell.unsplash = unsplash[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "def", sender: unsplash[indexPath.row].urls.small)
    }
 
    /*
     ScrollingDown Logic Goes here
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       // print("content size: \(scrollView.contentSize)")
        
        let scrollviewHeight = scrollView.bounds.height
        let yOffset = scrollView.contentOffset.y
        let contentSizeHeight = scrollView.contentSize.height
        
        let absoluteYOffset = yOffset + scrollviewHeight
        if absoluteYOffset > (contentSizeHeight - 100) {
            loadMoreData()
        }
    }
}
