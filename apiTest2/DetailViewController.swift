//
//  DetailViewController.swift
//  apiTest2
//
//  Created by Hamza on 1/23/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var imgDetail: UIImageView!
    @IBOutlet weak var heartButton: UIButton!
    var imageUrl: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageCallingFromUrl()
    }
    
    func imageCallingFromUrl(){
        if let imageUnwrappedUrl = imageUrl {
           //print(imageUnwrappedUrl)
           let url = URL(string: imageUnwrappedUrl)
           let data = try? Data(contentsOf: url!)
           if let imageData = data {
               let image = UIImage(data: imageData)
               imgDetail.image = image
           }else {
              print("image error")
           }
       }
    }
    
    @IBAction func heartPressed(_ sender: UIButton) {
        heartButton.isSelected = !heartButton.isSelected
        
        /*
        Alert Heart Click
        */
        if heartButton.isSelected {
            let alertController = UIAlertController(title: "Added to favorites ", message:
                "successfully", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "close", style: .default))
            self.present(alertController, animated: true, completion: nil)
            
        }
        else{
            let alertController = UIAlertController(title: "Removed from favorites ", message:
                "successfully", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "close", style: .default))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func shareButtonTapped(_ sender: UIButton) {
        
        if let imageUnwrappedUrl = imageUrl {
            let url = URL(string: imageUnwrappedUrl)
            let data = try? Data(contentsOf: url!)
            if let imageData = data {
            let image = UIImage(data: imageData)
                    /*
                     Share Activity Handler
                    */
            guard let imagepass = image else { return }
            let activityController = UIActivityViewController(activityItems: [imagepass], applicationActivities: nil)
            activityController.completionWithItemsHandler = { (nil, completed,  _, Error) in
                if completed {
                    print("completed")
                }
                else{
                    print("not completed")
                }
            }
            present(activityController , animated: true) {
                print("presented")
            }
                     
        } else {
            print("image error")
        }
        }
    }
    
    
}
