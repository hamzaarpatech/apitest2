//
//  Extension_ImageDownload.swift
//  apiTest2
//
//  Created by Hamza on 1/22/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import UIKit


var imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
   /*
     Image Caching goes here
     */
    func checkForImageCache(url: String) {
        if let cacheImage = imageCache.object(forKey: url as AnyObject) as? UIImage {
            self.image = cacheImage
            return
        }
        // download image if it is not cached
        self.downloadImage(from: url)
    }
    
    func downloadImage(from url: String) {
        guard let imageUrl = URL(string: url) else { return }
        URLSession.shared.dataTask(with: imageUrl) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async {
                self.image = image
                // cache image after download
                imageCache.setObject(image, forKey: url as AnyObject)
            }
        }.resume()
    }
}







