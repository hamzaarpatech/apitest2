//
//  Unsplash.swift
//  apiTest2
//
//  Created by Hassan Ahmed on 23/01/2020.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation

struct Unsplash : Decodable{
    let urls: UnsplashURL
       let id : String
    //  let instagram_username : String
}

struct UnsplashURL: Decodable {
    let regular: String
    let small: String
}
